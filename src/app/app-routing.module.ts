import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackageComponent } from './components/package/package.component';
import { PackageTypeComponent } from './components/package-type/package-type.component';


const routes: Routes = [
  {path: 'packages', component: PackageComponent},
  {path: 'package-types', component: PackageTypeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
