import { Component, OnInit } from '@angular/core';
import { Router, NavigationCancel, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  title = 'Home';

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.title = this.router.url;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationCancel || event instanceof NavigationEnd) {
        if (event.url === '/') {
          this.title = 'Home';
        } else {
          this.title = event.url.replace('/', '');
          this.title = this.title.replace('-', ' ');
          this.title = this.title.charAt(0).toUpperCase() + this.title.slice(1);
        }
      }
  });
  }
}
