import { Component, OnInit, Inject } from '@angular/core';
import { IPackageType } from 'src/app/utility/interfaces/package-type';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PackageTypeService } from 'src/app/utility/services/package-type.service';
import { EventService } from 'src/app/utility/services/event.service';

@Component({
  selector: 'app-add-update-package-type',
  templateUrl: './../add-update-package-type/add-update-package-type.component.html',
  styleUrls: ['./../add-update-package-type/add-update-package-type.component.scss']
})
export class EditPackageTypeComponent implements OnInit {
  packageType: IPackageType;
  packageTypeFormGroup: FormGroup;
  isEdit = true;

  constructor(public dialogRef: MatDialogRef<any>, private formBuilder: FormBuilder,
              private packageTypeService: PackageTypeService, private eventService: EventService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                this.packageTypeFormGroup = formBuilder.group({
                  type: new FormControl()
                });
              }

  ngOnInit(): void {
    this.getPackageType();
  }

  getPackageType() {
      this.packageTypeService.getPackageTypeById(this.data.id).subscribe(x => {
        console.log(x);
        this.packageTypeFormGroup = this.formBuilder.group({
            type: new FormControl(x.type)
        });
      }, error => {
          console.log(error);
      });
  }

  savePackageType() {
    if (this.packageTypeFormGroup.valid) {
      this.packageType = {
        id: this.data.id,
        type: this.packageTypeFormGroup.value.type
      };

      this.packageTypeService.updatePackageType(this.packageType).subscribe(() => {
        this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package Type updated successfully.'});
        this.dialogRef.close(true);
      }, error => {
        console.log(error);
        this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Error updating Package Type.'});
      });
    } else {
      this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Please senter required fields.'});
    }
  }

  closeModal() {
      this.dialogRef.close(false);
  }
}
