import { Component, OnInit, ViewChild } from '@angular/core';
import { PackageTypeService } from 'src/app/utility/services/package-type.service';
import { MatTableDataSource } from '@angular/material/table';
import { IPackageType } from 'src/app/utility/interfaces/package-type';
import { MatPaginator } from '@angular/material/paginator';
import { AddUpdatePackageTypeComponent } from './add-update-package-type/add-update-package-type.component';
import { MatDialog } from '@angular/material/dialog';
import { EventService } from 'src/app/utility/services/event.service';
import { EditPackageTypeComponent } from './edit-package-type/edit-package-type.component';

@Component({
  selector: 'app-package-type',
  templateUrl: './package-type.component.html',
  styleUrls: ['./package-type.component.scss']
})
export class PackageTypeComponent implements OnInit {
  dataSource = new MatTableDataSource<IPackageType>([]);
  displayedColumns: string[] = ['type', 'buttons'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private packageTypeService: PackageTypeService, public dialog: MatDialog,
              private eventService: EventService) { }

  ngOnInit(): void {
    this.getPackageTypes('');
  }

  getPackageTypes(searchTerm) {
    this.packageTypeService.getPackageTypes(searchTerm).subscribe(x => {
      this.dataSource = new MatTableDataSource<IPackageType>(x);
      this.dataSource.paginator = this.paginator;
    }, error => {
      console.log(error);
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  addPackageType(): void {
    const dialogRef = this.dialog.open(AddUpdatePackageTypeComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getPackageTypes('');
    });
  }

  editPackageType(id) {
    const dialogRef = this.dialog.open(EditPackageTypeComponent, {
      width: '400px',
      data: {id}
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getPackageTypes('');
    });
  }

  deletePackageType(id) {
    this.packageTypeService.deletePackageType(id).subscribe(() => {
      this.getPackageTypes('');
      this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package Type removed successfully.'});
    }, error => {
      this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Error removing package type.'});
      console.log(error);
    });
  }
}
