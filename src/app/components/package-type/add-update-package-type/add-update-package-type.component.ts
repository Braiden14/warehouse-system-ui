import { Component, OnInit } from '@angular/core';
import { IPackageType } from 'src/app/utility/interfaces/package-type';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { PackageTypeService } from 'src/app/utility/services/package-type.service';
import { EventService } from 'src/app/utility/services/event.service';

@Component({
  selector: 'app-add-update-package-type',
  templateUrl: './add-update-package-type.component.html',
  styleUrls: ['./add-update-package-type.component.scss']
})
export class AddUpdatePackageTypeComponent implements OnInit {
  packageType: IPackageType;
  packageTypeFormGroup: FormGroup;
  isEdit = false;

  constructor(public dialogRef: MatDialogRef<any>, formBuilder: FormBuilder,
              private packageTypeService: PackageTypeService, private eventService: EventService) {
                this.packageTypeFormGroup = formBuilder.group({
                  type: new FormControl()
                });
              }

  ngOnInit(): void {
  }

  savePackageType() {
    if (this.packageTypeFormGroup.valid) {
      this.packageType = {
        id: '531FB78D-3DD6-4973-B0C0-A0E245EB25E5',
        type: this.packageTypeFormGroup.value.type
      };

      this.packageTypeService.addPackageType(this.packageType).subscribe(x => {
        this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package Type added successfully.'});
        this.dialogRef.close();
      }, error => {
        console.log(error);
        this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Error adding Package Type.'});
      });
    } else {
      this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Please enter required fields.'});
    }
  }

  closeModal() {
    this.dialogRef.close(false);
  }
}
