import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdatePackageTypeComponent } from './add-update-package-type.component';

describe('AddUpdatePackageTypeComponent', () => {
  let component: AddUpdatePackageTypeComponent;
  let fixture: ComponentFixture<AddUpdatePackageTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdatePackageTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdatePackageTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
