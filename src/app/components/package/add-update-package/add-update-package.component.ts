import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

import { PackageTypeService } from '../../../utility/services/package-type.service';
import { PackageService } from '../../../utility/services/package.service';

import { IPackageType } from '../../../utility/interfaces/package-type';
import { IPackage } from '../../../utility/interfaces/package';
import { EventService } from 'src/app/utility/services/event.service';

@Component({
  selector: 'app-add-update-package',
  templateUrl: './add-update-package.component.html',
  styleUrls: ['./add-update-package.component.scss']
})
export class AddUpdatePackageComponent implements OnInit {
  package: IPackage;
  packageTypes: IPackageType[];
  isEdit = false;

  packageFormGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<any>, formBuilder: FormBuilder,
              private packageTypeService: PackageTypeService, private packageService: PackageService,
              private eventService: EventService) {
    this.packageFormGroup = formBuilder.group({
      type: new FormControl(),
      weight: new FormControl(),
      length: new FormControl(),
      height: new FormControl(),
      width: new FormControl(),
      quantity: new FormControl(),
      contents: new FormControl(),
      isFragile: new FormControl(false)
    });
   }

  ngOnInit(): void {
    this.packageTypeService.getPackageTypes('').subscribe(x => {
      this.packageTypes = x;
    });
  }

  savePackage() {
    if (this.packageFormGroup.valid) {
      this.package = {
        id: '531FB78D-3DD6-4973-B0C0-A0E245EB25E5',
        type: '',
        packageTypeId: this.packageFormGroup.value.type,
        weight: this.packageFormGroup.value.weight,
        length: this.packageFormGroup.value.length,
        height: this.packageFormGroup.value.height,
        width: this.packageFormGroup.value.width,
        quantity: this.packageFormGroup.value.quantity,
        contents: this.packageFormGroup.value.contents,
        isFragile: this.packageFormGroup.value.isFragile,
        volume: 0
      };

      this.packageService.addPackage(this.package).subscribe(() => {
        this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package added successfully.'});
        this.dialogRef.close(true);
      }, error => {
        console.log(error);
        this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Error adding package type.'});
      });
    } else {
      this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Please enter required fields.'});
    }
  }

  closeModal() {
    this.dialogRef.close(false);
  }
}
