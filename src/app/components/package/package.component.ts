import { Component, OnInit, ViewChild } from '@angular/core';
import { AddUpdatePackageComponent } from './add-update-package/add-update-package.component';

import { PackageService } from '../../utility/services/package.service';

import { IPackage } from '../../utility/interfaces/package';

import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { EventService } from 'src/app/utility/services/event.service';
import { EditPackageComponent } from './edit-package/edit-package.component';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements OnInit {
  displayedColumns: string[] = ['package-type', 'contents', 'weight', 'length', 'height'
  , 'width', 'quantity', 'fragile', 'volume', 'buttons'];

  searchTerm: string;
  dataSource = new MatTableDataSource<IPackage>([]);
  packages: IPackage[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  faEdit = faEdit;
  faTrash = faTrash;

  constructor(private packageService: PackageService, public dialog: MatDialog,
              private eventService: EventService) { }

  ngOnInit(): void {
    this.getPackages('');
  }

  private getPackages(searchTerm: string) {
    this.packageService.getPackages(searchTerm).subscribe(x => {
      this.dataSource = new MatTableDataSource<IPackage>(x);
      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource);
      this.packages = x;
    }, error => {
      console.log(error);
    });
  }

  addPackage(): void {
    const dialogRef = this.dialog.open(AddUpdatePackageComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getPackages('');
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  deletePackage(id) {
    this.packageService.deletePackage(id).subscribe(() => {
      this.getPackages('');
      this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package removed successfully.'});
    }, error => {
      this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Error removing package.'});
      console.log(error);
    });
  }

  editPackage(id) {
    const dialogRef = this.dialog.open(EditPackageComponent, {
      width: '400px',
      data: {id}
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getPackages('');
    });
  }
}
