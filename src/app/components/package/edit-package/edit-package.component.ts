import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { PackageTypeService } from '../../../utility/services/package-type.service';
import { PackageService } from '../../../utility/services/package.service';

import { IPackageType } from '../../../utility/interfaces/package-type';
import { IPackage } from '../../../utility/interfaces/package';
import { EventService } from 'src/app/utility/services/event.service';

@Component({
    selector: 'app-add-update-package',
    templateUrl: './../add-update-package/add-update-package.component.html',
    styleUrls: ['./../add-update-package/add-update-package.component.scss']
  })
  export class EditPackageComponent implements OnInit {
    package: IPackage;
    packageTypes: IPackageType[];
    edit = true;
    packageFormGroup: FormGroup;
    isEdit = true;

    constructor(private formBuilder: FormBuilder,
                private eventService: EventService, private packageService: PackageService,
                private packageTypeService: PackageTypeService,
                public dialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {
                  this.packageFormGroup = formBuilder.group({
                    type: new FormControl(''),
                    weight: new FormControl(''),
                    length: new FormControl(''),
                    height: new FormControl(''),
                    width: new FormControl(''),
                    quantity: new FormControl(''),
                    contents: new FormControl(''),
                    isFragile: new FormControl(false)
                  });
                }

    ngOnInit() {
      this.getPackage();
      this.getPackageTypes();
    }

    getPackage() {
      this.packageService.getPackageById(this.data.id).subscribe((response) => {
        this.packageFormGroup = this.formBuilder.group({
          type: new FormControl(response.packageTypeId),
          weight: new FormControl(response.weight),
          length: new FormControl(response.length),
          height: new FormControl(response.height),
          width: new FormControl(response.width),
          quantity: new FormControl(response.quantity),
          contents: new FormControl(response.contents),
          isFragile: new FormControl(response.isFragile)
        });
        }, (error) => console.log(error));
    }

    getPackageTypes() {
      this.packageTypeService.getPackageTypes('').subscribe((response) => {
          this.packageTypes = response;
         }, (error) => console.log(error));
    }

    savePackage() {
      if (this.packageFormGroup.valid) {
        this.package = {
          id: this.data.id,
          type: '',
          packageTypeId: this.packageFormGroup.value.type,
          weight: this.packageFormGroup.value.weight,
          length: this.packageFormGroup.value.length,
          height: this.packageFormGroup.value.height,
          width: this.packageFormGroup.value.width,
          quantity: this.packageFormGroup.value.quantity,
          contents: this.packageFormGroup.value.contents,
          isFragile: this.packageFormGroup.value.isFragile,
          volume: 0
        };

        this.packageService.updatePackage(this.package).subscribe(() => {
          this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Package Type added successfully.'});
          this.dialogRef.close(true);
        }, error => {
          console.log(error);
          this.eventService.announceGeneralNotification({isSuccessful: true, message: 'Error updating package.'});
        });
      } else {
        this.eventService.announceGeneralNotification({isSuccessful: false, message: 'Please enter required fields.'});
      }
    }

    closeModal() {
      this.dialogRef.close(false);
    }
  }
