export interface IPackageType {
    id: string;
    type: string;
}
