export interface IPackage {
   id: string;
   type: string;
   packageTypeId: string;
   weight: number;
   length: number;
   height: number;
   width: number;
   quantity: number;
   contents: string;
   isFragile: any;
   volume: number;
}
