import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { IPackage } from '../interfaces/package';

@Injectable({
    providedIn: 'root'
})
export class PackageService {
    apiUrl: string;

    httpOptions = {
        headers: new HttpHeaders()
    };

    constructor(private httpClient: HttpClient) {
        this.apiUrl = environment.apiUrl;
    }

    updateHeader() {
        this.httpOptions.headers = new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: `Bearer `,
          observe: 'response'
        });
    }

    getPackages(searchTerm) {
        this.updateHeader();
        return this.httpClient.get<IPackage[]>(`${this.apiUrl}Packages?searchterm${searchTerm}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }

    addPackage(body: IPackage) {
        this.updateHeader();
        return this.httpClient.post(`${this.apiUrl}Packages`, body, this.httpOptions);
    }

    updatePackage(body: IPackage) {
        this.updateHeader();
        return this.httpClient.put(`${this.apiUrl}Packages/${body.id}`, body, this.httpOptions).pipe(
          timeout(environment.timeout)
        );
    }

    getPackageById(id: string) {
        this.updateHeader();
        return this.httpClient.get<IPackage>(`${this.apiUrl}Packages/${id}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }

    deletePackage(id: string) {
        this.updateHeader();
        return this.httpClient.delete(`${this.apiUrl}Packages/${id}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }
}
