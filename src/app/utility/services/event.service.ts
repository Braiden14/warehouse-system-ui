import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable()
export class EventService {
    private generalNotificationSource = new Subject<any>();

    generalNotification$ = this.generalNotificationSource.asObservable();

    announceGeneralNotification(generalNotification) {
        this.generalNotificationSource.next(generalNotification);
    }
}
