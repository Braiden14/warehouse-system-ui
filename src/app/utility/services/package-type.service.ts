import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { IPackage } from '../interfaces/package';
import { IPackageType } from '../interfaces/package-type';

@Injectable({
    providedIn: 'root'
})
export class PackageTypeService {
    apiUrl: string;

    httpOptions = {
        headers: new HttpHeaders()
    };

    constructor(private httpClient: HttpClient) {
        this.apiUrl = environment.apiUrl;
    }

    updateHeader() {
        this.httpOptions.headers = new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: `Bearer `,
          observe: 'response'
        });
    }

    getPackageTypes(searchTerm) {
        this.updateHeader();
        return this.httpClient.get<IPackage[]>(`${this.apiUrl}PackageType?searchterm${searchTerm}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }

    addPackageType(body: IPackageType) {
        this.updateHeader();
        return this.httpClient.post(`${this.apiUrl}PackageType`, body, this.httpOptions);
    }

    updatePackageType(body: IPackageType) {
        this.updateHeader();
        return this.httpClient.put(`${this.apiUrl}PackageType/${body.id}`, body, this.httpOptions).pipe(
          timeout(environment.timeout)
        );
    }

    getPackageTypeById(id: string) {
        this.updateHeader();
        return this.httpClient.get<IPackageType>(`${this.apiUrl}PackageType/${id}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }

    deletePackageType(id: string) {
        this.updateHeader();
        return this.httpClient.delete(`${this.apiUrl}PackageType/${id}`
          , this.httpOptions)
          .pipe(
            timeout(environment.timeout)
        );
    }
}
