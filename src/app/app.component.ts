import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {MatSnackBar} from '@angular/material/snack-bar';
import { SnackbarMessageComponent } from './components/snackbar-message/snackbar-message.component';
import { EventService } from './utility/services/event.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  drawerWidth = 304;
  currentDrawerWidth = this.drawerWidth;

  constructor(public route: ActivatedRoute, private snackbar: MatSnackBar,
              private eventService: EventService) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    this.eventService.generalNotification$.subscribe(notification => {
      this.displayNotification(notification.isSuccessful, notification.message);
    });
  }

  displayNotification(isSuccessful, message) {
    if (isSuccessful) {
      this.snackbar.openFromComponent(
        SnackbarMessageComponent, {
        data: message,
        duration: 5000,
        panelClass: 'success-snackbar',
        verticalPosition: 'bottom'
      });
    } else {
      this.snackbar.openFromComponent(
        SnackbarMessageComponent, {
        data: message,
        duration: 5000,
        panelClass: 'error-snackbar',
        verticalPosition: 'bottom'
      });
    }
  }

  toggleDrawerWidth() {
    if (this.currentDrawerWidth === this.drawerWidth) {
      this.currentDrawerWidth = 92;
    } else {
      this.currentDrawerWidth = this.drawerWidth;
    }
  }
}
