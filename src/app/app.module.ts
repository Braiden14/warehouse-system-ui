import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PackageComponent } from './components/package/package.component';
import { PackageTypeComponent } from './components/package-type/package-type.component';
import { AddUpdatePackageComponent } from './components/package/add-update-package/add-update-package.component';
import { EditPackageComponent } from './components/package/edit-package/edit-package.component';
import { AddUpdatePackageTypeComponent } from './components/package-type/add-update-package-type/add-update-package-type.component';
import { EditPackageTypeComponent } from './components/package-type/edit-package-type/edit-package-type.component';

import { PackageService } from './utility/services/package.service';
import { PackageTypeService } from './utility/services/package-type.service';
import { EventService } from './utility/services/event.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { SnackbarMessageComponent } from './components/snackbar-message/snackbar-message.component';

@NgModule({
  declarations: [
    AppComponent,
    PackageComponent,
    PackageTypeComponent,
    AddUpdatePackageComponent,
    TopBarComponent,
    SnackbarMessageComponent,
    EditPackageComponent,
    AddUpdatePackageTypeComponent,
    EditPackageTypeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSelectModule,
    NgbModule,
    FontAwesomeModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatCardModule,
    MatTooltipModule
  ],
  providers: [
    PackageService,
    PackageTypeService,
    EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
